<?php
namespace StringTransform;

use StringTransform\StringHelper;
use StringTransform\StringValidator;

final class StringTransform
{
    protected $output = [];

    public function __construct()
    {
        $this->validator = StringValidator::make();
        $this->stringTransformMethods = get_class_methods(StringHelper::class);
    }

    private function getInputFromUser()
    {
        return trim(fread(STDIN, 100));
    }

    private function executeStringTransform($val)
    {
        foreach ($this->stringTransformMethods as $method) {
            $this->output[] = call_user_func(array(StringHelper::class, $method), $val);
        }
    }

    public function reset()
    {
        $this->output = [];
    }

    public function run($input)
    {
        $input = trim($input);
        $this->reset();

        if ($this->validator->validate($input)) {
            $this->executeStringTransform($input);
        }

        return empty($this->validator->message());
    }

    public function getOutput()
    {
        return implode(PHP_EOL, $this->output);
    }

    public function message()
    {
        return implode(' ,', $this->validator->message());
    }

    public function consoleRun()
    {
        while (true) {
            echo "Enter a string: ";
            $val = StringTransform::getInputFromUser();

            if ($this->run($val)) {
                echo $this->getOutput() . PHP_EOL;
            } else {
                echo "Your input has the following errors: " . PHP_EOL;
                echo $this->message() . PHP_EOL;
            }

            echo "Do you want to try again? Type 'yes' to continue: ";

            if (strtolower(StringTransform::getInputFromUser()) !== 'yes') {
                break;
            }
        }
    }
}
