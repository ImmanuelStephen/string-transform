<?php
namespace StringTransform;

final class StringHelper
{
    public static function toUppper($val)
    {
        return strtoupper($val);
    }

    public static function alternateUpper($val)
    {
        for ($pos = 0; $pos < strlen($val); $pos++) {
            if ($pos % 2 == 1) {
                $val[$pos] = strtoupper($val[$pos]);
            } else {
                $val[$pos] = strtolower($val[$pos]);
            }
        }

        return $val;
    }

    public static function toCsv($val)
    {
        $file = new \SplFileObject('file.csv', 'w');

        if (!$file->fputcsv(str_split($val, 1))) {
            throw new \Exception('Data cannot be written to file');
        }
        $file = null;

        return 'CSV Created!';
    }
}
