<?php
namespace StringTransform;

final class StringValidator
{
    protected $valid = true;
    protected $messages = [];

    protected function validateNotEmpty($val)
    {
        if (strlen(trim($val)) === 0) {
            $this->valid = false;
            $this->messages[] = "String cannot be empty";
        }
    }

    protected function validateHasOnlyAlphabets($val)
    {
        if (preg_match('/^[ a-zA-Z]*$/', $val) === 0) {
            $this->valid = false;
            $this->messages[] = "String must contain only alphabets";
        }
    }

    private function reset()
    {
        $this->valid = true;
        $this->messages = [];
    }

    public function validate($val)
    {
        $this->reset();
        $validatorMethods = preg_grep('/^validate\w+$/', get_class_methods($this));

        foreach ($validatorMethods as $method) {
            $this->$method($val);
        }

        return $this->valid;
    }

    public function message()
    {
        return $this->messages;
    }

    public static function make()
    {
        return (new StringValidator());
    }
}
