# StringFormatter

This program can transform a string into different formats. Below are the available formats
eg. Given the input 'hello world'

The program will produce the following output

1. UpperCase - HELLO WORLD
1. Alternate Uppercase - hElLo wOrLd
1. Create CSV file by breaking string into characters

## Installation

1. Install [composer](https://getcomposer.org/download/) 
1. Go to the project root directory
1. Run ```composer install & composer dump-autoload```

## How to run the program

1. Go to project root directory
1. Run ```php main.php```

## How to run the tests

1. Goto project root directory
1. Run ```vendor/bin/phpunit```
