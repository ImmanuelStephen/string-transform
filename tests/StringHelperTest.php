<?php
namespace StringTransform\StringTransformTest;

use PHPUnit\Framework\TestCase;
use StringTransform\StringHelper;

class StringHelperTest extends TestCase
{
    public function testUpperCase()
    {
        $this->assertEquals('HELLO WORLD', StringHelper::toUppper('hello world'));       
    }

    public function testAlternateCase()
    {
        $this->assertEquals('hElLo wOrLd', StringHelper::alternateUpper('HELLO WORLD'));       
    }

    public function testToCsv()
    {
        StringHelper::toCsv('HELLo WORLD');
        $handle = fopen('file.csv', 'r');
        $this->assertEquals(str_split('HELLo WORLD', 1), fgetcsv($handle));      
        fclose($handle); 
    }
}