<?php

namespace StringTransform\StringTransformTest;

use PHPUnit\Framework\TestCase;
use StringTransform\StringTransform;

class StringTransformTest extends TestCase
{
    public function setup()
    {
        $this->program = new StringTransform();
    }

    public function testValidUserInput()
    {
        $this->assertTrue($this->program->run('hello world'));
        $this->assertEquals(
            'HELLO WORLD' . PHP_EOL . 'hElLo wOrLd' . PHP_EOL . 'CSV Created!', $this->program->getOutput()
        );
    }

    public function testValidUserInputWithTrailingSpaces()
    {
        $this->assertTrue($this->program->run(' hello world '));
        $this->assertEquals(
            'HELLO WORLD' . PHP_EOL . 'hElLo wOrLd' . PHP_EOL . 'CSV Created!', $this->program->getOutput()
        );
    }

    public function testInvalidInput()
    {
        $this->assertFalse($this->program->run('hello world12$!@#$%^&*'));
        $this->assertEmpty($this->program->getOutput());
        $this->assertEquals("String must contain only alphabets", $this->program->message());
    }

    public function testEmptyInput()
    {
        $this->assertFalse($this->program->run(' '));
        $this->assertEmpty($this->program->getOutput());
        $this->assertEquals("String cannot be empty", $this->program->message());
    }
}
