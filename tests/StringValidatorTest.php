<?php
namespace StringTransform\StringTransformTest;

use PHPUnit\Framework\TestCase;
use StringTransform\StringValidator;

class StringValidatorTest extends TestCase
{
    public function setup()
    {
        $this->validator = StringValidator::make();
    }

    public function testValidString()
    {
        $this->assertTrue(StringValidator::make()->validate('hello world'));
    }

    public function testInValidString()
    {
        $this->assertFalse($this->validator->validate('hello world12'));
        $this->assertEquals(['String must contain only alphabets'], $this->validator->message());
    }

    public function testEmptyString()
    {
        $this->assertFalse($this->validator->validate('    '));
        $this->assertEquals(['String cannot be empty'], $this->validator->message());
    }
}
